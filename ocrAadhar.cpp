#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <iostream>
#include <vector>
#include <opencv2/text.hpp>

#include <algorithm> 
#include <cctype> 
#include <fstream>
#include <ctype.h>

using namespace cv;
using namespace std;
using namespace cv::text;

Mat img;

bool all_digits(const string& s)
{
  return ((s.find_first_not_of("0123456789") == std::string::npos) && (s.length()==4));
}

string returnName(vector<Rect> boxes,vector<float> confidences,vector<string> words)
{
	string name="";
	for(int i=0;i<words.size()-1;i++)
	{
		char first=words[i][0];
		char second=words[i+1][0];
		if (isupper(first) && isupper(second))
		{
			name=words[i]+" "+words[i+1];
		}
	}

	return name;

}


string returnDOB(vector<Rect> boxes,vector<float> confidences,vector<string> words)
{
	// cout<<"Possible Candidates for DOB"<<endl;
	string word;
	string currectWord="11/11/1989";
	for(int i=0;i<words.size();i++)
	{
		word=words[i];
		if(words[i].length()==10&&(word[6]=='1')&&(word[7]=='9'))
		{
			word[2]='/';
			word[5]='/';
			// cout<<word<<endl;
			currectWord=word;
		}

	}

	return currectWord;
}

string returnSex(vector<Rect> boxes,vector<float> confidences,vector<string> words)
{
	string defaultSex="Male";
	for(int i=0;i<words.size();i++)
	{
		if(words[i]=="Female")
		{
			defaultSex="Female";
		}
	}

	return defaultSex;
}

string returnState(vector<Rect> boxes,vector<float> confidences,vector<string> words)
{
	string defaultState="TN";
	for(int i=0;i<words.size();i++)
	{
		if(words[i]=="Andhra"||words[i]=="Pradesh"||words[i]=="Andh")
		{
			defaultState="AP";
		}
	}

	return defaultState;

}


// string returnAadharnumber(vector<Rect> boxes,vector<float> confidences,vector<string> words)
// {
// 	vector <Rect> numerical_Boxes;
// 	vector <float> numerical_Confidences;
// 	vector <string> numerical_words;
// 	vector<int> textHeight;
// 	vector<int> textPosition;
// 	vector<int> textXcoordinate;
	
// 	for(int i=0;i<words.size();i++)
// 	{
// 		string singleWord=words[i];
// 		if(all_digits(singleWord))
// 		{
// 			numerical_words.push_back(words[i]);
// 			numerical_Boxes.push_back(boxes[i]);
// 			numerical_Confidences.push_back(confidences[i]);
// 			textHeight.push_back(boxes[i].tl().y);
// 			textPosition.push_back(boxes[i].height);
// 			textXcoordinate.push_back(boxes[i].tl().x);
// 		}

// 	}
	
// 	for(int i=0;i<numerical_words.size();i++)
// 	{

// 	}


// 	// sort(textHeight.begin(), textHeight.end());
// 	// sort(textPosition.begin(),textPosition.end());



// 	// for(int i=0;i<numerical_words.size();i++)
// 	// {

// 	// }

// 	string output="";
// 	if(numerical_words.size()>=3)
// 	{
// 		output=numerical_words[0]+numerical_words[1]+numerical_words[2];
// 	}
// 	// else
// 	// {
// 	// 	for(int i=0;i<numerical_words.size();i++)
// 	// 	{
// 	// 		output=output=numerical_words[i];
// 	// 	}
// 	// }

// 	return output;

// }

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        cout << "Please give the location of source image as ./test /path/to/sourceImage" <<endl;
        return -1;
    }
    img =imread(argv[1]);
    if (img.empty())
    {
        cout << "No Image found found at the given location,please try again" <<endl;
        return -1;
    }

    Mat gray;
    // namedWindow("Original Image",CV_WINDOW_NORMAL);
    // imshow("Original Image",img);
    cvtColor(img, gray, CV_BGR2GRAY);

    string output;

    vector<Rect>   boxes;
    vector<string> words;
    vector<float>  confidences;
    Ptr<OCRTesseract> ocr = OCRTesseract::create();
    
    ocr->run(gray, output, &boxes, &words, &confidences, OCR_LEVEL_WORD);

    Scalar color=Scalar(255,0,0);

    float scale_img  = 600.f/img.rows;
    float scale_font = (float)(2-scale_img)/1.4f;

	// namedWindow("lines detected",CV_WINDOW_NORMAL);

    for(int i=0;i<boxes.size();i++)
    {
		rectangle(img, boxes[i].tl(), boxes[i].br(), Scalar(255,0,255),3);
		Size word_size = getTextSize(words[i], FONT_HERSHEY_SIMPLEX, (double)scale_font, (int)(3*scale_font), NULL);
		rectangle(img, boxes[i].tl()-Point(3,word_size.height+3), boxes[i].tl()+Point(word_size.width,0), Scalar(255,0,255),-1);
		putText(img, words[i],boxes[i].tl()-Point(1,1), FONT_HERSHEY_SIMPLEX, scale_font, Scalar(255,255,255),(int)(3*scale_font));

    }

    string DOB=returnDOB(boxes,confidences,words);
	// string aadharnuber=returnAadharnumber(boxes,confidences,words);
	string sex=returnSex(boxes,confidences,words);
	string state=returnState(boxes,confidences,words);

	string name=returnName(boxes,confidences,words);
	ofstream myfile;
	myfile.open ("retrieved.txt");
	myfile << DOB;
	myfile<<"\n";
	myfile<<sex;
	myfile<<"\n";
	myfile<<state;
	myfile<<"\n";
	myfile<<name;
	myfile<<"\n";
	myfile.close();

	ofstream myfile2;
	myfile2.open("parsing.txt");
	myfile2<<output;
	myfile2.close();

	// imshow("lines detected",img);
	// waitKey(0);

	// cout<<output<<endl;

    
    //imwrite("lines.jpg",img);
    //return 0;
}
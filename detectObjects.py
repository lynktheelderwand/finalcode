import cv2
import numpy as numpy
import sys
import numpy as np

img=cv2.imread(sys.argv[1],0)
ret2,th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

kernel = np.ones((5,5),np.uint8)
th2 = cv2.bitwise_not(th2)
th2 = cv2.dilate(th2,kernel,iterations = 3)
dist_transform = cv2.distanceTransform(th2,cv2.DIST_L2,5)
ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)
cv2.namedWindow("Thresholded Image",cv2.WINDOW_NORMAL)
cv2.namedWindow("Original Image",cv2.WINDOW_NORMAL)
cv2.imshow("Thresholded Image",sure_fg)
cv2.imshow("Original Image",img)
cv2.waitKey(0)
# img=cv2.imread(sys.argv[1],0)
# H,W=img.shape
# print(img.shape)
# cv2.namedWindow("Original Image",cv2.WINDOW_NORMAL)
# th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,501,2)
# cv2.imshow("Original Image",th3)
# cv2.waitKey(0)

# im2, contours, hierarchy = cv2.findContours(th3,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

# for cnt in contours:
# 	area = cv2.contourArea(cnt)
# 	if(area>1000 and area<(W*W)/4):
# 		cv2.drawContours(th3, [cnt],0,-1)


# cv2.namedWindow("Contour Filled Image",cv2.WINDOW_NORMAL)
# cv2.imshow("Contour Filled Image",th3)
# cv2.waitKey(0)
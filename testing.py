import cv2
import numpy as np
import sys

img=cv2.imread(sys.argv[1],0)
sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=7)
sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=7)

answerx=np.sum(sobelx[:,:])
answery=np.sum(sobely[:,:])

print(answerx)
print(answery)

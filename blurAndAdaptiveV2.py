import cv2
import sys

img=cv2.imread(sys.argv[1],0)

blurParameter=3
AdaptiveThresholdParameter=9
if(len(sys.argv)==3):
	blurParameter=sys.argv[2]
if(len(sys.argv)==4):
	AdaptiveThreshold=sys.argv[3]

img = cv2.medianBlur(img,7)
img = cv2.medianBlur(img,5)
img = cv2.medianBlur(img,3)

th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,AdaptiveThresholdParameter,2)

cv2.imwrite("processed2V"+sys.argv[1],th3)
